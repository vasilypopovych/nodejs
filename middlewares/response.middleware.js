
const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query


  if (req.middelewareRes === "Creating validation user done") {
    res.status(200).send(`New user created.`);
    next();
  } else if (req.middelewareRes === "Creating validation user false") {
    res.status(400).send({
      error: true,
      message: `User entity to create isn't valid`,
    });
  } else if (req.middelewareRes === "Updating validation user done") {
    res.status(200).send(`User info updated`);
    next();
  } else if (req.middelewareRes === "Updating validation user false") {
    res.status(400).send({
      error: true,
      message: `User entity to update isn't valid`,
    });
  } else if (req.middelewareRes === "Creating validation fighter done") {
    res.status(200).send(`New fighter created.`);
    next();
  } else if (req.middelewareRes === "Creating validation fighter false") {
    res.status(400).send({
      error: true,
      message: `Fighter entity to create isn't valid`,
    });
  } else if (req.middelewareRes === "Updating validation fighter done") {
    res.status(200).send(`Fighter info updated`);
    next();
  } else if (req.middelewareRes === "Updating validation fighter false") {
    res.status(400).send({
      error: true,
      message: `Fighter entity to update isn't valid`,
    });
  }
};

exports.responseMiddleware = responseMiddleware;
