const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    const isPresentFields =
    !!req.body.name &&
    !!req.body.power &&
    !!req.body.defense;

    const isBodyId = !req.body.id;

    let isNeedlessFields = true;
  for (prop of Object.keys(req.body)) {
    if (!Object.keys(fighter).includes(prop)) {
      isNeedlessFields = false;
    }
  }

    const power =  typeof req.body.power === 'number' && req.body.power >= 1 && req.body.power <= 100;
    const defense = typeof req.body.defense === 'number' && req.body.defense >= 1 && req.body.defense <= 10;
    const health  = typeof req.body.health === 'number' && req.body.health >= 80 && req.body.health <= 120;

    const propertyFormat = power && defense && health;


    if (isPresentFields && isBodyId && propertyFormat && isNeedlessFields) {
      req.middelewareRes = 'Creating validation fighter done',
      next();
    } else {
      req.middelewareRes = 'Creating validation fighter false',
      next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    /* При обновленні вже не валідуються  формати данних*/

    const isPresentFields =
    !!req.body.name ||
    !!req.body.power ||
    !!req.body.defense;
    
    const isBodyId = !req.body.id;

    let isNeedlessFields = true;
  for (prop of Object.keys(req.body)) {
    if (!Object.keys(fighter).includes(prop)) {
      isNeedlessFields = false;
    }
  }

    const power =  typeof req.body.power === 'number' && req.body.power >= 1 && req.body.power <= 100;
    const defense = typeof req.body.defense === 'number' && req.body.defense >= 1 && req.body.defense <= 10;
    const health  = typeof req.body.health === 'number' && req.body.health >= 80 && req.body.health <= 120;

    const propertyFormat = power && defense && health;


    if (isPresentFields && isBodyId && propertyFormat && isNeedlessFields) {
      req.middelewareRes = 'Updating validation fighter done',
      next();
    } else {
      req.middelewareRes = 'Updating validation fighter false',
      next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;