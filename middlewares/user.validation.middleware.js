const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  const isPresentFields =
    !!req.body.firstName &&
    !!req.body.lastName &&
    !!req.body.email &&
    !!req.body.phoneNumber &&
    !!req.body.password;

  //Id в body запитів має бути відсутнім
  const isBodyId = !req.body.id;

  // Наявність зайвих властивостей(не з папки models) - заборонено.
  let isNeedlessFields = true;
  for (prop of Object.keys(req.body)) {
    if (!Object.keys(user).includes(prop)) {
      isNeedlessFields = false;
    }
  }

  //створити константу для перевірки кожної властивості і потім всі констатни обєднати в одну propertyFormat
  const onlyGmail = req.body.email.indexOf("gmail") != -1;
  //повинен починатись з +380 і містити 13 символів
  const phoneNumber =
    req.body.phoneNumber.length === 13 &&
    req.body.phoneNumber.indexOf("+380") === 0;
  //строка, min 3 символи
  const password =
    typeof req.body.password === "string" && req.body.password.length >= 3;

  const propertyFormat = onlyGmail && phoneNumber && password;

  if (isPresentFields && isBodyId && propertyFormat && isNeedlessFields) {
    req.middelewareRes = 'Creating validation user done',
    next();
  } else {
    req.middelewareRes = 'Creating validation user false',
    next();
  }

};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  /* При обновленні вже не валідуються  формати данних*/

  const isPresentFields =
    !!req.body.firstName ||
    !!req.body.lastName ||
    !!req.body.email ||
    !!req.body.phoneNumber ||
    !!req.body.password;

    //Id в body запитів має бути відсутнім
  const isBodyId = !req.body.id;

  // Наявність зайвих властивостей(не з папки models) - заборонено.
  let isNeedlessFields = true;
  for (prop of Object.keys(req.body)) {
    if (!Object.keys(user).includes(prop)) {
      isNeedlessFields = false;
    }
  }

  //створити константу для перевірки кожної властивості і потім всі констатни обєднати в одну propertyFormat
  const onlyGmail = req.body.email.indexOf("gmail") != -1;
  //повинен починатись з +380 і містити 13 символів
  const phoneNumber =
    req.body.phoneNumber.length === 13 &&
    req.body.phoneNumber.indexOf("+380") === 0;
  //строка, min 3 символи
  const password =
    typeof req.body.password === "string" && req.body.password.length >= 3;

  const propertyFormat = onlyGmail && phoneNumber && password;

  if (isPresentFields && isBodyId && propertyFormat && isNeedlessFields) {
    req.middelewareRes = 'Updating validation user done',
    next();
  } else {
    req.middelewareRes = 'Updating validation user false',
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
