const express = require('express')
const cors = require('cors');
require('./config/db');

const app = express()



app.use(cors());
app.use(express.json()); // потрібен для запитів ПОСТ і ПУТ щоб розпізнавати тіло запиту як обєкт json
app.use(express.urlencoded({ extended: true })); // потрібен для запитів ПОСТ і ПУТ щоб розпізнавати тіло запиту як строку або масив

const routes = require('./routes/index');
routes(app);


app.use('/', express.static('./client/build'));


const port = 3050;

app.listen(port, (e) => {
    if(e){
        console.log(`Помилка в app.listen: ${e}`);
    }
});

exports.app = app;