const { Router } = require("express");
const UserService = require("../services/userService");
const { createUserValid, updateUserValid } = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");


const router = Router();
// TODO: Implement route controllers for user

//GET ALL
router.get("/", (req, res) => {
  res.status(200).json(UserService.getAllUsers());
});

//GET BY ID
router.get("/:id", (req, res) => {
  const user = UserService.getUserById({ id: `${req.params.id.slice(3)}` });
  if (user) {
    res.status(200).json(user);
  } else {
    res.status(404).json({
      error: true,
      message: `User not found`,
    });
  }
});

//DELETE
router.delete("/:id", (req, res) => {
  res.status(200).send("Deleted user with id:" + req.params.id.slice(3));
  UserService.deleteUser(req.params.id.slice(3));
});

//POST
router.post("/", createUserValid, responseMiddleware, (req, res) => {
  UserService.createUser(req.body);
});

//PUT
router.put("/:id", updateUserValid, responseMiddleware, (req, res) => {
  UserService.updateUser(req.params.id.slice(3), req.body);
});

module.exports = router;
