const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter


//GET ALL
router.get("/", (req, res) => {
    res.status(200).json(FighterService.getAllFighters());
});

//GET BY ID
router.get('/:id', (req, res) => {
    let fighter = FighterService.getFighterById({"id": `${req.params.id.slice(3)}`});
    if (fighter) {
        res.status(200).json(fighter);
      } else {
        res.status(404).json({
          error: true,
          message: `Fighter not found`,
        });
      }
});

//DELETE
router.delete('/:id', (req, res) => {
    res.status(200).send('Deleted fighter with id:' + req.params.id.slice(3))
    FighterService.deleteFighter(req.params.id.slice(3))
});

//POST
router.post("/", createFighterValid, responseMiddleware, (req, res) => {
    FighterService.createFighter(req.body);
    });

//PUT
router.put("/:id", updateFighterValid, responseMiddleware, (req, res) => {
    FighterService.updateFighter(req.params.id.slice(3), req.body)
    
    });
    
module.exports = router;