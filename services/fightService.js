const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
/* 
- Використовувати методи з репозторія файтера, а не юзера.
- Щоб не було повтору значень в БД  напевно треба реалізувати методи фільтрації в цьому файлі
*/
    getAllFighters() {
        return UserRepository.getAll();
    }

    getFighterById(value) {
        return UserRepository.getOne(value);
    }

    
    createFighter(data) {
        this.data = UserRepository.create(data)
    }

    deleteFighter(id) {
        return UserRepository.delete(id)
    }

    updateFighter(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate)
    }
}

module.exports = new FightersService();