const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAllUsers() {
        return UserRepository.getAll();
    }

    getUserById(value) {
        return UserRepository.getOne(value);
    }

    
    createUser(data) {
        this.data = UserRepository.create(data)
    }

    deleteUser(id) {
        return UserRepository.delete(id)
    }

    updateUser(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate)
    }

}


module.exports = new UserService(); //звідси експортується екземпляр класу!!!

