const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAllFighters() {
        return FighterRepository.getAll();
    }

    getFighterById(value) {
        return FighterRepository.getOne(value);
    }

    
    createFighter(data) {
        this.data = FighterRepository.create(data)
    }

    deleteFighter(id) {
        return FighterRepository.delete(id)
    }

    updateFighter(id, dataToUpdate) {
        return FighterRepository.update(id, dataToUpdate)
    }
}

module.exports = new FighterService();